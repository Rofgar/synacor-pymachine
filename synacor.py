import sys
import struct
import inspect

class Syncanor:
    lower_literal_boundry = 0
    upper_literal_boundry = 32767

    lower_register_index = 32768
    upper_register_index = 32775

    def __init__(self):
        self.registers = {}
        self.stack = []
        self.memory = []
        self.execution_index = 0
        self.step_by_step = False
        self.verbose = False

    def load(self, instructions):
        for chunk in chunks(instructions, 2):
            value, = struct.unpack('<H', chunk)
            self.memory.append(value)
        
        if len(self.memory) > self.upper_literal_boundry:
            print('Loaded program is too large, max size is', self.upper_literal_boundry)
            sys.exit(-10)

        print('Loaded program size of:', len(self.memory))

    def execute(self):
        self.execution_index = 0
        instructions_size = len(self.memory)
        while self.execution_index < instructions_size:
            operation_code = self.memory[self.execution_index]
            operation = self.ops.get(operation_code)
            if not operation:
                print('Unsupported instruction code', operation_code, '[', chr(operation_code), ']')
                sys.exit(-2)   

            operation_arguments = len(inspect.signature(operation).parameters)
            # We start from 1 to skip the self argument
            arguments = []
            for _ in range(1, operation_arguments):
                self.execution_index += 1
                if self.execution_index >= instructions_size:
                    print('Was searching for an argument', operation_arguments, 'for', operation, 'but instructions ended')
                    sys.exit(-3)
                arguments.append(self.memory[self.execution_index])
            
            # Increment execution index early, since operations like jmp may alter it
            self.execution_index += 1

            if not operation.__name__ == 'out':
                index_str: str = '[' + str(self.execution_index - 1 - len(arguments)) + ']'
                op_name = operation.__name__
                if len(arguments) > 0:
                    op_name += ':' 
                if self.verbose:
                    print(index_str, op_name, ', '.join([str(i) for i in arguments]))

            if self.step_by_step:
                print('Execution index', self.execution_index)
                print('Registers', self.registers)
                memory_resolve = {}
                for v in self.registers.values():
                    try:
                        memory_resolve[v] = self.memory[v]
                    except IndexError:
                        print('Failed to find', v)
                        memory_resolve[v] = 0
                print('Memory resolve', memory_resolve)
                print('Stack', self.stack)

                command = input()
                if command == 'continue':
                    self.step_by_step = False
                elif command.startswith('jump'):
                    self.jmp(int(command.split()[1]))
                    continue
                elif command != '' and self.is_valid_literal_number(int(command)):
                    print(self.memory[int(command)])
                elif command != '' and self.is_valid_register_index(int(command)):
                    print(self.registers[int(command)])

            if arguments:
                operation(self, *arguments)
            else:
                operation(self)

    def is_valid_literal_number(self, a):
        return a >= self.lower_literal_boundry and a <= self.upper_literal_boundry

    def validate_literal_number(self, a):
        if not self.is_valid_literal_number(a):
            print('Expected a unsigned intger literal', self.lower_literal_boundry, 'to', self.upper_literal_boundry, 'wrong value given', a)
            self.halt()

    def is_valid_register_index(self, a):
        return a >= self.lower_register_index and a <= self.upper_register_index

    def validate_register_index(self, a):
        if not self.is_valid_register_index(a):
            print('There are only 8 registers available, indexed from', self.lower_register_index, 'to', self.upper_register_index, 'invalid register index', a)
            self.halt()

    def lookup_value(self, value):
        if self.is_valid_register_index(value):
            return self.registers.get(value, 0)
        elif self.is_valid_literal_number(value):
            return value
        else:
            print('Invalid argument', value)
            sys.exit(-4)

    def set(self, a, b):
        self.validate_register_index(a)
        value = self.lookup_value(b)
        self.registers[a] = value

    def push(self, a):
        value = self.lookup_value(a)
        self.stack.append(value)

    def pop(self, a):
        self.validate_register_index(a)
        self.registers[a] = self.stack.pop()
        
    def eq(self, a, b, c):
        self.validate_register_index(a)
        b_value = self.lookup_value(b)
        c_value = self.lookup_value(c)

        if b_value == c_value:
            self.registers[a] = 1
        else:
            self.registers[a] = 0

    def gt(self, a, b, c):
        self.validate_register_index(a)
        b_value = self.lookup_value(b)
        c_value = self.lookup_value(c)
        if b_value > c_value:
            self.registers[a] = 1
        else:
            self.registers[a] = 0

    def out(self, a):
        value = self.lookup_value(a)
        if value:
            print(chr(a),  end='')
        else:
            print('Invalid argument for output instruction')
            sys.exit(-4)

    def jmp(self, a):
        value = self.lookup_value(a)
        if value > 0 and value < len(self.memory):
            self.execution_index = value
        else:
            print('Invalid address', value)    
            sys.exit(-5)

    def jt(self, a, b):
        value = self.lookup_value(a)
        if value != 0:
            self.jmp(b)

    def jf(self, a, b):
        value = self.lookup_value(a)
        if value == 0:
            self.jmp(b)

    def add(self, a, b, c):
        self.validate_register_index(a)

        sum = self.lookup_value(b) + self.lookup_value(c)
        mod = sum % (self.upper_literal_boundry + 1)
        self.set(a, mod)

    def mult(self, a, b, c):
        self.validate_register_index(a)

        multiplication = self.lookup_value(b) * self.lookup_value(c)
        mod = multiplication % (self.upper_literal_boundry + 1)
        self.set(a, mod)

    def mod(self, a, b, c):
        self.validate_register_index(a)
        self.set(a, self.lookup_value(b) % self.lookup_value(c))

    def and_op(self, a, b, c):
        self.validate_register_index(a)
        b_value = self.lookup_value(b)
        c_value = self.lookup_value(c)
        self.registers[a] = b_value & c_value

    def or_op(self, a, b, c):
        self.validate_register_index(a)
        b_value = self.lookup_value(b)
        c_value = self.lookup_value(c)
        self.registers[a] = b_value | c_value

    def not_op(self, a, b):
        self.validate_register_index(a)
        value = self.lookup_value(b)
        packed_value = struct.pack('<H', value)
        inverted_value = bytearray([i ^ 0xFF for i in packed_value])
        inverted_value[1] = inverted_value[1] & 0x7F
        unpacked_value, = struct.unpack('<H', inverted_value)
        self.registers[a] = unpacked_value

    def call(self, a):
        next_index = self.execution_index
        if next_index < len(self.memory):
            self.stack.append(next_index)
            self.jmp(a)
        else:
            print('There is no next instruction to be saved to stack')
            sys.exit(-7)

    def rmem(self, a, b):
        self.validate_register_index(a)
        self.registers[a] = self.memory[self.lookup_value(b)]

    def wmem(self, a, b):
        a_value = self.lookup_value(a)
        b_value = self.lookup_value(b)
        self.memory[a_value] = b_value

    def ret(self):
        if self.stack:
            value = self.stack.pop()
            self.jmp(value)
        else:
            self.halt() 

    def in_op(self, a):
        value = input()
        ords = [ord(char) for char in value]
        if self.is_valid_register_index(a):
            self.registers[a] = ords
        else:
            self.memory[a] = ords

    def halt(self):
        sys.exit()

    def noop(self):
        pass

    ops = {
        0: halt,
        1: set,
        2: push,
        3: pop,
        4: eq,
        5: gt,
        6: jmp,
        7: jt,
        8: jf,
        9: add,
        10: mult,
        11: mod,
        12: and_op,
        13: or_op,
        14: not_op,
        15: rmem,
        16: wmem,
        17: call,
        18: ret,
        19: out,
        20: in_op,
        21: noop
    }

def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

def print_help_and_exit():
    print('You have to provide a path to instructions file as an argument')
    sys.exit(-1)

if __name__ == '__main__':
    print('Welcome to Synacor Virtual Machine\n')
    if len(sys.argv) < 2:
        print_help_and_exit()

    instructions_file = sys.argv[1]
    if not instructions_file:
        print_help_and_exit()

    instructions = []
    with open(instructions_file, 'rb') as f:
        instructions = f.read()

    syncanor = Syncanor()
    syncanor.load(instructions)
    syncanor.execute()
